require 'net/imap'
require 'mail'

class IMAP::Retriever
  def initialize(service)
    @service = service
  end

  def fetch(limit=nil)
    connection = _connect
    connection.select("Reports")
    if limit.nil?
      connection.uid_search(["NOT", "DELETED"]).each do |uid|
        body = connection.uid_fetch(uid, ['RFC822']).first.attr['RFC822']
        message = IMAP::Parser.new(body)
        message.parse
        if message.valid
          message.process
          connection.uid_move(uid, "Reports/succeeded")
        else
          connection.uid_move(uid, "Reports/failed")
        end
      end
    else
      connection.uid_search(["NOT", "DELETED"]).first(limit).each do |uid|
        body = connection.uid_fetch(uid, ['RFC822']).first.attr['RFC822']
        message = IMAP::Parser.new(body)
        message.parse
        puts message.type
        puts message.valid
        puts message.file
        if message.valid
          message.process
          connection.uid_move(uid, "Reports/succeeded")
        else
          connection.uid_move(uid, "Reports/failed")
        end
      end
    end
    connection.expunge
    connection.close
  end

  def reset
    connection = _connect
    connection.select("Reports/failed")
    connection.uid_search(["NOT", "DELETED"]).each do |uid|
      connection.uid_move(uid, "Reports")
    end
    connection.select("Reports/succeeded")
    connection.uid_search(["NOT", "DELETED"]).each do |uid|
      connection.uid_move(uid, "Reports")
    end
    connection.expunge
    connection.close
  end

  private
  def _connect
    imap = Net::IMAP.new(@service.host, @service.port, @service.port.equal?(993))
    imap.starttls() unless @service.port == 993
    imap.login(@service.secure_cred.username, @service.secure_cred.password)
    return imap
  end
end
