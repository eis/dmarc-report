require 'mail'

class IMAP::Parser
  attr_accessor :msg, :valid, :type, :file

  def initialize(raw_source)
    self.msg = Mail.read_from_string(raw_source)
    self.valid = nil
    self.type = nil
    self.file = nil
  end

  def parse
    attachment_part = nil
    if self.msg.has_attachments?
      attachment_part = self.msg.attachments.first
    elsif msg.filename.present?
      attachment_part = self.msg
    end
    _validate_attachment(attachment_part.filename)
    if self.valid
      self.file = StringIO.new(attachment_part.body.decoded)
    end
  end

  def process
    if self.valid
      dr = Dmarc::Report.new(self)
      Report.save_dmarc(dr)
    end
  end

  private

  def _validate_attachment(filename)
    gzip = Regexp.union(
      [
        # Gzip file
        /\.xml\.gz$/,
        /\.xml\.gzip/
      ]
    )
    zip = Regexp.union(
      [
        # Zip
        /\.zip/
      ]
    )
    # Some MTAs are stupid and put spaces in the filename.
    filename = filename.gsub(/\s+/, "")
    if filename.match?(gzip)
      self.type = 'gzip'
      self.valid = true
    elsif filename.match?(zip)
      self.type = 'zip'
      self.valid = true
    else
      self.type = 'unsupported'
      self.valid = false
    end
  end

end
