class DmarcJob

  def self.queue_name
    name.demodulize.underscore
  end

  def before job
    log 'starting'
    @start_time = Time.now
  end

  def error(job, exception)
    log "Caught error, may retry.. #{exception}"
    GC.start(full_mark: true)
  end

  def failure job
    log "run failed: #{job.last_error}", :error
    begin
      ErrorMailer.job_error(job).deliver_now
    rescue => e
      log "Failed to deliver email about failure: #{e}"
    end
  end

  def max_attempts
    2
  end

  def success
    log "finished after #{(Time.now - @start_time).to_i} seconds"
    GC.start(full_mark: true)
  end

  def queue_name
    self.class.queue_name
  end

  private

  def log msg, level=:info
    ResourceLogger.send level, "#{log_name}: #{msg}"
  end

  def log_name
    self.class.name.demodulize
  end

end
