require 'forwardable'

module ResourceLogger
  extend SingleForwardable
  def_delegators :@logger, :add, :debug, :error, :fatal, :info, :log, :unknown, :warn
  def_delegators :@logger, :level, :level=, :sev_threshold

  @logger = Logger.new(Rails.root.join('log', 'resource.log').to_s)
  @logger.formatter = proc do |severity, time, progname, msg|
    "%s| %s | %s | %s: %s\n" % [time.strftime("%Y-%m-%d %H:%M:%S"),severity, ENV['HOSTNAME'], Process.pid, msg]
  end
end
