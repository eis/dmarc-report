require 'maxmind/db'

class GeoLite

  def initialize(type)
    @reader = MaxMind::DB.new("vendor/GeoIp/#{type}.mmdb", mode: MaxMind::DB::MODE_MEMORY)
  end

  def search_ip(ip)
    return @reader.get(ip)
  end
end
