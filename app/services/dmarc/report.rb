require 'date'
require 'zip'
require 'zlib'

module Dmarc
  class Report
    def initialize(msg)
      @doc = Nokogiri::XML.parse(decompress(msg))
      @mm_asn = GeoLite.new('GeoLite2-ASN')
      @mm_city = GeoLite.new('GeoLite2-City')
    end

    def decompress(msg)
      case msg.type
      when 'zip'
        unzip(msg.file)
      when 'gzip'
        gunzip(msg.file)
      end
    end

    def unzip(file)
      xml = String.new()
      Zip::File.open_buffer(file) do |zip_file|
        zip_file.each do |entry|
          puts "Extracting #{entry.name}"
          xml = entry.get_input_stream.read
        end
      end
      return xml
    end

    def gunzip(file)
      xml = String.new()
      gz = Zlib::GzipReader.new(file)
      xml = gz.read
      #Zlib::GzipReader.open(file.read) { |gz|
      #  xml = gz.read
      #}
      return xml
    end

    def metadata
      data = @doc.at_xpath('//report_metadata')
      metadata = {}
      [ :org_name, :email, :extra_contact_info, :report_id ].each do |attr|
        metadata[attr] = data.at_xpath("#{attr}/text()").to_s
      end
      metadata[:begin] = DateTime.strptime(data.at_xpath("date_range/begin/text()").to_s, "%s")
      metadata[:end] = DateTime.strptime(data.at_xpath("date_range/end/text()").to_s, "%s")

      return metadata
    end

    def policy_published
      data = @doc.at_xpath('//policy_published')
      policy_published = {}
      [ :domain, :adkim, :aspf, :p, :pct, :sp ].each do |attr|
        policy_published[attr] = data.at_xpath("#{attr}/text()").to_s
      end

      return policy_published
    end

    def records
      # Map attributes schema for each record.
      attrs = {
        source_ip: "row/source_ip",
        count: "row/count",
        disposition: "row/policy_evaluated/disposition",
        dkim: "row/policy_evaluated/dkim",
        spf: "row/policy_evaluated/spf",
        reason_type: "row/policy_evaluated/reason/type",
        comment: "row/policy_evaluated/reason/comment",
        header_from: "identifiers/header_from",
        dkim_domain: "auth_results/dkim/domain",
        dkim_result: "auth_results/dkim/result",
        #dkim_hresult: "auth_results/dkim/human_result",
        spf_domain: "auth_results/spf/domain",
        spf_result: "auth_results/spf/result"
      }

      @doc.xpath('//record').map do |record|
        # Create empty data container for records.
        record_attrs = {}

        # Map well-defined attrs to Record model schema
        attrs.map do |attr, path|
          record_attrs[attr] = record.at_xpath("#{path}/text()").to_s
        end

        # Map ASN data to Record model schema
        @mm_asn.search_ip(record_attrs[:source_ip]).try :map do |attr, value|
          record_attrs[attr.to_sym] = value
        end

        geoip_parse(record_attrs[:source_ip]).try :map do |attr, value|
          record_attrs[attr.to_sym] = value
        end
        record_attrs
      end
    end

    def geoip_parse(ip)
      mm_result = @mm_city.search_ip(ip)
      if not mm_result.nil?
        result = Hash.new()
        result['city'] = mm_result.dig('city','names', 'en')
        result['continent_code'] = mm_result.dig('continent', 'code')
        result['continent_name'] = mm_result.dig('continent', 'names', 'en')
        result['country_iso_code'] = mm_result.dig('country', 'iso_code')
        result['country_name'] = mm_result.dig('country', 'names', 'en')
        result['registered_country_iso_code'] = mm_result.dig('registered_country', 'iso_code')
        result['registered_country_name'] = mm_result.dig('registered_country', 'names', 'en')
        result['latitude'] = mm_result.dig('location', 'latitude')
        result['longitude'] = mm_result.dig('location', 'longitude')
        result['accuracy_radius'] = mm_result.dig('location', 'accuracy_radius')
      end
      return result
    end
  end
end
