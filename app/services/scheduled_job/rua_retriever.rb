class ScheduledJob::RuaRetriever < ScheduledJob::Base

  def self.frequency
    5.minutes
  end

  def self.run_offset
    0.minutes #Every 10 minutes starting on top of the hour
  end

  def perform
    services = Service.where(protocol: :IMAP, active: true, tag: 'rua')
    services.each do |service|
      ResourceLogger.info "Scheduled Job for service #{service.tag}"
      IMAP::Retriever.new(service).fetch(1)
    end
  end
end
