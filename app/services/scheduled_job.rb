module ScheduledJob

  # Make sure all scheduled jobs on disk are queued up.
  # Can be called via rails runner
  def self.queue_all_jobs
    jobs_dir = Rails.root.join('app/services/scheduled_job')
    Dir["#{jobs_dir}/*.rb"].sort.reverse.each do |path|
      name = "#{File.basename(path, '.rb')}"
      klass = "ScheduledJob::" + name.camelize
      klass.constantize.schedule_run
    end
  end

  # job parent object to handle auto-rescheduling based on the added methods
  # ::run_minute and ::run_hour
  class Base < DmarcJob

    # Overwrite me..
    # This is how often, in seconds this job should run
    # def self.frequency
    #   raise NotImplemented
    # end

    def self.priority
      0
    end

    # overwrite me.. This is the offset (in seconds) from the standard frequency
    # ie if frequency is 1.day and run_offset is 20.minutes, the job will run
    # at 12:20am every day.
    # def self.run_offset
    #   0
    # end

    # Makes sure normal scheduled run is in place
    def self.schedule_run
      # Use Time.now+1 - in case the current job finished in 0 seconds, it won't
      # get rescheduled.  This occurred during testing, so it might happen in
      # real life
      pending = Delayed::Job.where(run_at: (Time.now+1)..(Time.now+frequency)).where(queue: queue_name)
      if pending.empty?
        # No future runs are scheduled, so schedule one
        now = Time.now
        # time_t is nicely zeroed for UTC.  In order to make sure every 2 hours
        # keeps at 2am/4am/6am/etc in local time, we create a UTC time instance
        # with the same day/hour/etc as our local instance.  We do the math
        # pretending we're in UTC, then we create a local time instance with
        # the same values of the UTC result as our scheduled time.
        # In addition to being nicely zeroed, this cleans up DST handling.
        utc_now = Time.utc(now.year, now.month, now.day, now.hour, now.min, now.sec)
        utc_start = utc_now - (utc_now.to_i % frequency) + run_offset
        if utc_now >= utc_start
          utc_start += frequency
        end
        next_start = Time.local(utc_start.year, utc_start.month, utc_start.day, utc_start.hour, utc_start.min, utc_start.sec)

        Delayed::Job.enqueue new(), run_at: next_start, priority: priority
      end
    end

    def failure job
      super
      self.class.schedule_run
    end

    def success
      super
      self.class.schedule_run
    end

  end

end
