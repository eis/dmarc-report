module DNS
  class DMARC
    attr_accessor :record, :adkim, :aspf, :fo, :p, :pct, :rf, :ri, :rua, :ruf, :sp, :v
    def initialize(domain)
      @resolver = DNS::Resolve.new()
      @record = @resolver.dmarc(domain)
      @adkim = 'r'
      @aspf = 'r'
      @fo = '0'
      @p = ''
      @pct = '100'
      @rf = 'afrf'
      @ri = '86400'
      @rua = ''
      @ruf = ''
      @sp = ''
      @v = ''
      if @record.present?
        self.parse(@record)
      end
    end

    def parse(record)
      record.split(';').each do |entry|
        case entry.strip
        when /^adkim/
          @adkim = entry.split('=', 2).last
        when /^aspf=/
          @aspf = entry.split('=', 2).last
        when /^fo=/
          @fo = entry.split('=', 2).last
        when /^p=/
          @p = entry.split('=', 2).last
          if @sp.empty?
            @sp = entry.split('=', 2).last
          end
        when /^rf=/
          @rf = entry.split('=', 2).last
        when /^ri=/
          @ri = entry.split('=', 2).last
        when /^rua=/
          @rua = entry.split('=', 2).last
        when /^ruf=/
          @ruf = entry.split('=', 2).last
        when /^sp=/
          @sp = entry.split('=', 2).last
        when /^v=/
          @v = entry.split('=', 2).last
        else
          puts "skipping #{entry}"
          next
        end
      end
    end
  end
end
