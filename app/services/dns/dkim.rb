module DNS
  class DKIM
    attr_accessor :record, :p, :k, :h, :v, :t, :n, :g
    def initialize(domain, selector)
      @resolver = DNS::Resolve.new()
      @record = @resolver.dkim(domain, selector)
      @p = ""
      @k = "rsa"
      @h = ""
      @v = "DKIM1"
      @t = ""
      @n = ""
      @g = ""
      if @record.present?
        self.parse(@record)
      end
    end

    def parse(record)
      record.split(';').each do |entry|
        case entry.strip
        when /^p=/
          @p = entry.split('=', 2).last
        when /^k=/
          @k = entry.split('=', 2).last
        when /^h=/
          @h = entry.split('=', 2).last
        when /^v=/
          @v = entry.split('=', 2).last
        when /^t=/
          @t = entry.split('=', 2).last
        when /^n=/
          @n = entry.split('=', 2).last
        when /^g=/
          @g = entry.split('=', 2).last
        else
          next
        end
      end
    end
  end
end
