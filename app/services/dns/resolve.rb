require 'dnsruby'
module DNS
  class Resolve

    def initialize(nameserver=['1.1.1.1'])
      @resolver = Dnsruby::Resolver.new(nameserver: nameserver)
    end

    def spf(domain)
      txts = self.txt(domain)
      result = nil
      if txts
        txts.answer.each do |rr|
          result = rr.data if rr.data.start_with?('v=spf')
        end
      end
      return result
    end

    def dkim(domain, selector)
      result = nil
      # Build query string from selector and domain
      dkim_query = "#{selector}._domainkey.#{domain}"
      # Check for CNAME first.
      cname = self.cname(dkim_query)

      if cname
        txts = self.txt(cname.answer.first.rdata.to_s)
      else
        txts = self.txt(dkim_query)
      end

      if txts
        txts.answer.each do |rr|
          result = rr.data if rr.data.match?('p=') and rr.data.match?(';')
        end
      end
      return result
    end

    def dmarc(domain)
      result = nil
      txts = self.txt("_dmarc.#{domain}")
      if txts
        txts.answer.each do |rr|
          result = rr.data if rr.data.start_with?('v=DMARC1')
        end
      end
      result
    end

    def cname(domain)
      begin
        return @resolver.query(domain, Dnsruby::Types.CNAME)
      rescue Dnsruby::ResolvError => e
        puts e
        return nil
      end
    end

    def a(domain)
      begin
        return @resolver.query(domain, Dnsruby::Types.A)
      rescue Dnsruby::ResolvError => e
        puts e
        return nil
      end
    end

    def aaaa(domain)
      begin
        return @resolver.query(domain, Dnsruby::Types.AAAA)
      rescue Dnsruby::ResolvError => e
        puts e
        return nil
      end
    end

    def ptr(ip)
      begin
        return @resolver.query(ip, Dnsruby::Types.PTR)
      rescue Dnsruby::ResolvError => e
        puts e
        return nil
      end
    end

    # Returns an array of TXT records or nil for any ResolvErrors
    def txt(domain)
      begin
        return @resolver.query(domain, Dnsruby::Types.TXT)
      rescue Dnsruby::ResolvError => e
        puts e
        return nil
      end
    end

  end
end
