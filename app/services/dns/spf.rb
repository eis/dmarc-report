module DNS
  class SPF
    attr_accessor :record, :a, :includes, :ip4, :ip6, :mx, :v
    def initialize(domain)
      @resolver = DNS::Resolve.new()
      @record = @resolver.spf(domain)
      @ip4 = Array.new()
      @ip6 = Array.new()
      @includes = Array.new()
      @includes_parser = Array.new()
      @mx = false
      @a = false
      @v = ''
      if not @record.nil?
        self.parse(@record)
        self.recurse_includes
      end
    end

    def parse(record)
      record.split(' ').each do |entry|
        case entry.strip
        when /^ip4/
          @ip4.push(entry.split(':', 2).last)
        when /^ip6/
          @ip6.push(entry.split(':', 2).last)
        when /^include/
          @includes.push(entry.split(':', 2).last)
          @includes_parser.push(entry.split(':', 2).last)
        when /^mx/
          @mx = true
        when /^a/
          @a = true
        when /^v/
          @v = entry.split('=', 2).last
        else
          next
        end
      end
    end

    def recurse_includes()
      while not @includes_parser.empty?()
        spf = @resolver.spf(@includes_parser.shift())
        self.parse(spf)
      end
    end

  end
end
