// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
$(document).on('ready turbolinks:load', function() {
  var url = document.location.toString();
  if (url.match('#')) {
      $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
  }

  // Change hash for page-reload
  $('.nav-tabs a').on('shown.bs.tab', function (e) {
      window.location.hash = e.target.hash;
  });

  $('#secure-creds-table').DataTable({
    "retrieve": true,
    "autoWidth": false,
    "pageLength": 5,
    "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
    "order": [ [ 0, 'desc' ] ],
    "columnDefs": [ {
      "targets": -1,
      "orderable": false
    } ]
  });

  $('#services-table').DataTable({
    "retrieve": true,
    "autoWidth": false,
    "pageLength": 5,
    "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
    "order": [ [ 0, 'desc' ] ],
    "columnDefs": [ {
      "targets": -1,
      "orderable": false
    } ]
  });

  $('#networks-table').DataTable({
    "retrieve": true,
    "autoWidth": false,
    "pageLength": 5,
    "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
    "order": [ [ 2, 'asc' ] ],
    "columnDefs": [ {
      "targets": -1,
      "orderable": false
    } ]
  });


});
