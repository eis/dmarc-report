// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on('ready turbolinks:load', function() {
  $('#records').DataTable( {
    ajax: {
        url: $(this).data("table-url"),
        dataSrc: ''
    },
    columns: [
      {data: 'source_ip_string', "width": "5%"},
      {data: 'header_from', "width": "5%"},
      //{data: 'spf_domain'},
      {data: 'spf', "width": "5%"},
      //{data: 'dkim_domain'},
      {data: 'dkim', "width": "5%"},
      {data: 'autonomous_system', "width": "20%"}
    ],
    deferRender: true,
    //serverSide: true,
    processing: true,
    autowidth: true
  });
});
