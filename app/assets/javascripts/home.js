// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on('ready turbolinks:load', function() {
  google.charts.load('current', {
    packages: ['corechart', 'geochart', 'bar', 'table'],
    'mapsApiKey': 'AIzaSyDr6q3xVlL4BUWLB_Ce0I94vndUPhH2W-A'
  });
  var chart_divs = document.getElementsByClassName('chart');
  for (let item of chart_divs) {
    var graph_type = $(item).data('graph-type')
    if (graph_type == 'bar') {
      google.charts.setOnLoadCallback(function() {
        draw_barchart(item.id);
      });
    } else if (graph_type == 'geochart') {
      google.charts.setOnLoadCallback(function() {
        draw_geochart(item.id);
      });
    } else if (graph_type == 'pie')  {
      google.charts.setOnLoadCallback(function() {
        draw_piechart(item.id);
      });
    }
  }
});

function draw_barchart(element_id) {
  var graph_div = document.getElementById(element_id);
  var options = $(graph_div).data("graph-options");
  var view_def = $(graph_div).data("graph-view");
  var pagination = $(graph_div).data("graph-pagination");
  $.getJSON(graph_div.getAttribute("data-graph-data"), function(data) {
    var g_data_table = new google.visualization.arrayToDataTable(data);
    var g_data_view = new google.visualization.DataView(g_data_table);
    g_data_view.setColumns(view_def);
    var g_chart = new google.visualization.BarChart(graph_div);
    if (pagination['enabled']) {
      EnableBarChartPagination(g_chart, g_data_table, g_data_view, options, pagination.pageSize, document.getElementById('prev_button'), document.getElementById('next_button'));
    } else {
      g_chart.draw(g_data_view, options);
      google.visualization.events.addListener(chart, 'select', function() {
        var selection = chart.getSelection();
        if (selection.length > 0 && selection[0]['row'] != null)
          window.open(view.getValue(selection[0]['row'], 2));
          chart.setSelection();
      });
    }
  }).fail(function() {
    $('#route_error').show();
    return;
  });
}

function EnableBarChartPagination(chart, data_table, view, options, ps, prevButton, nextButton) {
  var currentPage = -1;
  var pageSize = ps;
  // Pad the datatable to have an exact number of pages, otherwise the bars'
  // size will be artificially increased
  var dt = data_table;
  if (dt.getNumberOfRows() % pageSize != 0) {
    for (var i = pageSize - (dt.getNumberOfRows() % pageSize); i > 0; i--) {
      dt.addRow(['', 0, '']);
    }
  }

  var paginate = function(dir) {
    var numRows = data_table.getNumberOfRows();
    currentPage += dir;
    var rows = [];
    for (var i = pageSize*currentPage; i < pageSize*(currentPage+1) && i < numRows; i++) {
      rows.push(i);
    }
    view.setRows(rows);
    chart.clearChart();
    chart.draw(view, options);
    currentPage == 0 ? prevButton.setAttribute('disabled', 'disabled') : prevButton.removeAttribute('disabled');
    currentPage == numRows/pageSize-1 ? nextButton.setAttribute('disabled', 'disabled') : nextButton.removeAttribute('disabled');
    google.visualization.events.addListener(chart, 'select', function() {
      var selection = chart.getSelection();
      if (selection.length > 0 && selection[0]['row'] != null)
        window.open(view.getValue(selection[0]['row'], 2));
        chart.setSelection();
    });
  }

  prevButton.onclick = function() { paginate(-1) };
  nextButton.onclick = function() { paginate(1) };
  paginate(1);
}

function draw_geochart(element_id) {
  var graph_div = document.getElementById(element_id);
  //var options = $(graph_div).data("graph-options");

  $.getJSON(graph_div.getAttribute("data-graph-data"), function(data) {
    var g_data_table = new google.visualization.arrayToDataTable(data);
    var options = {
      colorAxis: {
        colors: ['#F3F2F1','#012169']
      },
      displayMode: 'region',
      tooltip: {
        textStyle: {
          color: '#FF0000',
          showColorCode: true
        },
        trigger: 'focus'
      }
    }

    var g_chart = new google.visualization.GeoChart(graph_div);
    g_chart.draw(g_data_table, options);
    google.visualization.events.addListener(g_chart, 'regionClick', function(data) {
      window.open("/country/" + data.region);
      g_chart.setSelection();
    });
  }).fail(function() {
    $('#route_error').show();
    return;
  });
}

function draw_piechart(element_id) {
  var graph_div = document.getElementById(element_id);
  var options = $(graph_div).data("graph-options");
  $.getJSON(graph_div.getAttribute("data-graph-data"), function(data) {
    var g_data_table = new google.visualization.arrayToDataTable(data);
    var g_chart = new google.visualization.PieChart(graph_div);
    g_chart.draw(g_data_table, options);
  }).fail(function() {
    $('#route_error').show();
    return;
  });
}
