// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on('ready turbolinks:load', function() {
  $('#from-headers').DataTable({
    "retrieve": true,
    "autoWidth": false,
    "pageLength": 20,
    "lengthMenu": [[20, 40, -1], [20, 40, "All"]],
    "order": [ [ 1, 'desc' ]]
  });
});
