// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on('ready turbolinks:load', function() {
  // Draw datatable for ASN Index
  $('#asns').DataTable({
    "retrieve": true,
    "autoWidth": false,
    "pageLength": 20,
    "lengthMenu": [[20, 40, -1], [20, 40, "All"]],
    "order": [ [ 1, 'desc' ]],
    "deferRender": true
  });

  // Toggle chevron right/down
  $('a.collapsable').click(function () {
    $(this).find('span').toggleClass('glyphicon-chevron-right').toggleClass('glyphicon-chevron-down');
  });

  // On panel show, redraw tables as datatables.
  $('.panel-collapse').on('show.bs.collapse', function () {
    var tables = $(this).find('table');
    var search_bar = $(this).find('input');
    var dtables = [];
    for (let table of tables) {

      dtables.push($('#' + table.id).DataTable({
        "retrieve": true,
        "pageLength": 10,
        "lengthChange": false,
        "order": [ [ 1, 'desc' ]],
        "dom": 'lrtip'
      }));
      $(search_bar).keyup(function(){
        for (let dtable of dtables) {
          dtable.search($(this).val()).draw();
        }
      })
    }
  });
});
