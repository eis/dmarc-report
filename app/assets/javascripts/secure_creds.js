// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
function secure_creds_toggle_text_area() {
  var pass_fields = $('#secure_creds_form input[type="password"]');
  var text_area_fields = $('#secure_creds_form textarea');
  if ($('#secure_creds_form #secure_cred_is_key').is(':checked')) {
    pass_fields.prop('disabled', true);
    pass_fields.hide();
    text_area_fields.prop('disabled', false);
    text_area_fields.show();
  } else {
    text_area_fields.prop('disabled', true);
    text_area_fields.hide();
    pass_fields.prop('disabled', false);
    pass_fields.show();
  }
}

$(document).on('page:change', function() {

  if ( $('#secure_creds_form').length ) {
    secure_creds_toggle_text_area();
    $('#secure_creds_form #secure_cred_is_key').change(secure_creds_toggle_text_area);
  }

});
