// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).on('ready turbolinks:load', function() {
  $('[data-toggle="tooltip"]').tooltip();

  // Draw datatable for ASN Index
  $('#records').DataTable({
    "retrieve": true,
    "autoWidth": true,
    "pageLength": 10,
    "lengthMenu": [[10, 20, 40, -1], [10, 20, 40, "All"]],
    "ordering": false,
    "deferRender": true
  });
});
