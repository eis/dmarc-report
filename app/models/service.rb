class Service < ApplicationRecord
  belongs_to :secure_cred

  enum protocol: [:IMAP, :SMTP]
end
