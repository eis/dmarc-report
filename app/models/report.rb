class Report < ApplicationRecord
  has_many :records, dependent: :delete_all
  has_one :policy_published, dependent: :delete
  before_save :fix_epoch_ms

  def self.save_dmarc(dmarc)
    report = Report.create(dmarc.metadata)
    report.create_policy_published(dmarc.policy_published)
    dmarc.records.each do |row|
      report.records << Record.create(row)
    end
    report.update_stats
  end

  def update_counts
    counts = self.records.inject({failed: 0, passed: 0}) do |sum, record|
      key = (record.disposition == 'reject') ? :failed : :passed
      sum[key] = sum[key] + record.count
      sum
    end
    self.update_attributes!({failed_count: counts[:failed], passed_count: counts[:passed]})
  end

  def update_asn_spf_stats
    self.records.distinct(:asn).pluck(:asn, :aso).each do |asn, aso|
      org = AutonomousSystem.find_or_initialize_by(number: asn)
      org.name = aso
      org.spf_summary = {} unless org.spf_summary.present?
      self.records.by_asn(asn).group(:header_from, :spf_domain, :spf_result).pluck(:header_from, :spf_domain, :spf_result, Arel.sql('SUM(count)')).each do |header_from, spf_domain, spf_result, count|
        if not org.spf_summary.key?(header_from)
          org.spf_summary[header_from] = HashWithIndifferentAccess.new({})
        end
        if not org.spf_summary[header_from].key?(spf_domain)
            org.spf_summary[header_from][spf_domain] = HashWithIndifferentAccess.new({
                "softfail": 0,
                "neutral": 0,
                "permerror": 0,
                "none": 0,
                "temperror": 0,
                "pass": 0,
                "fail": 0
            })
        end
        org.spf_summary[header_from][spf_domain][spf_result] += count
      end
      org.save
    end
  end

  def update_asn_dkim_stats
    self.records.distinct(:asn).pluck(:asn, :aso).each do |asn, aso|
      org = AutonomousSystem.find_or_initialize_by(number: asn.to_i)
      org.name = aso
      org.dkim_summary = {} unless org.dkim_summary.present?
      self.records.by_asn(asn).group(:header_from, :dkim_domain, :dkim_result).pluck(:header_from, :dkim_domain, :dkim_result, Arel.sql('SUM(count)')).each do |header_from, dkim_domain, dkim_result, count|
        if not org.dkim_summary.key?(header_from)
          org.dkim_summary[header_from] = HashWithIndifferentAccess.new({})
        end
        if not org.dkim_summary[header_from].key?(dkim_domain)
          org.dkim_summary[header_from][dkim_domain] = HashWithIndifferentAccess.new({
              "pass": 0,
              "fail": 0,
              "unsigned": 0
            })
        end
        org.dkim_summary[header_from][dkim_domain][dkim_result] += count
      end
      org.save
    end
  end

  def update_stats
    self.records.distinct(:asn).pluck(:asn, :aso).each do |asn, aso|
      org = AutonomousSystem.find_or_initialize_by(number: asn)
      org.name = aso if org.name.nil?
      org.spf_summary = {} unless org.spf_summary.present?
      org.dkim_summary = {} unless org.dkim_summary.present?
      #org.aggregate_summary = {} unless org.aggregate_summary.present?
      records = self.records.by_asn(asn)
      records.group(:header_from, :spf_domain, :spf_result).pluck(:header_from, :spf_domain, :spf_result, Arel.sql('SUM(count)')).each do |header_from, spf_domain, spf_result, count|
        if not org.spf_summary.key?(header_from)
          org.spf_summary[header_from] = HashWithIndifferentAccess.new({})
        end
        if not org.spf_summary[header_from].key?(spf_domain)
            org.spf_summary[header_from][spf_domain] = HashWithIndifferentAccess.new({
                "softfail": 0,
                "neutral": 0,
                "permerror": 0,
                "none": 0,
                "temperror": 0,
                "pass": 0,
                "fail": 0
            })
        end
        org.spf_summary[header_from][spf_domain][spf_result] += count
      end
      records.group(:header_from, :dkim_domain, :dkim_result).pluck(:header_from, :dkim_domain, :dkim_result, Arel.sql('SUM(count)')).each do |header_from, dkim_domain, dkim_result, count|
        if not org.dkim_summary.key?(header_from)
          org.dkim_summary[header_from] = HashWithIndifferentAccess.new({})
        end
        if not org.dkim_summary[header_from].key?(dkim_domain)
          org.dkim_summary[header_from][dkim_domain] = HashWithIndifferentAccess.new({
              "pass": 0,
              "fail": 0,
              "none": 0,
              'policy': 0,
              'neutral': 0,
              'temperror': 0,
              'permerror': 0
            })
        end
        puts header_from
        puts dkim_domain
        puts dkim_result
        puts count
        puts ''
        org.dkim_summary[header_from][dkim_domain][dkim_result] += count
      end
        org.save
    end
  end

  private
  # Callbacks
  def fix_epoch_ms
    # This must be millisecond epoch, not second epoch
    if self.begin > DateTime.now
      self.begin = Time.zone.at(self.begin.to_i/1000)
      self.end = Time.zone.at(self.end.to_i/1000)
    end
  end
end
