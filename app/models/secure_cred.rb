class SecureCred < ApplicationRecord
  self.primary_key = :id

  has_many :service, dependent: :destroy

  attr_encrypted :password, key: Rails.application.credentials[Rails.env.to_sym][:db_sc], mode: :per_attribute_iv_and_salt

  validates :password, confirmation: true, if: :encrypted_password_changed?
  validates :password_confirmation, presence: true, if: :encrypted_password_changed?

  before_save :reset_salt_and_iv

  private

  def reset_salt_and_iv
    if encrypted_password_changed?
      p = password
      self.encrypted_password_salt = nil
      self.encrypted_password_iv = nil
      self.password = p
    end
  end
end
