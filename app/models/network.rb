class Network < ApplicationRecord
  enum status: [:unknown, :blessed, :permitted, :disallowed]
  enum source: [:spf_record, :user]
end
