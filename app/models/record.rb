class Record < ApplicationRecord
  include Filterable
  # Report model relations
  belongs_to :report
  belongs_to :network, optional: true
  before_save :downcase_fields
  before_save :fix_dkim
  before_save :fix_spf
  before_save :predict_dmarc

  alias_attribute :asn, :autonomous_system_number
  alias_attribute :aso, :autonomous_system_organization

  # Set up calculated fields for annotation in Google Charts
  attr_accessor :autonomous_system, :gmaps_url, :source_ip_string

  def autonomous_system
    "#{self.aso} (#{self.asn})"
  end

  def gmaps_url
    case self.accuracy_radius.to_i
    when 1..50
      zoom = '20z'
    when 51..200
      zoom = '15z'
    when 200..1000
      zoom = '5z'
    end
    self.gmaps_url = URI.escape("https://www.google.com/maps/@#{self.latitude},#{self.longitude},#{zoom}")
  end

  def source_ip_string
    self.source_ip_string = self.source_ip_before_type_cast
  end

  # Search Scopes for filterable
  search_scope :dkim_domain, -> (domain) {
    where(dkim_domain: domain)
  }

  search_scope :dkim_result, -> (dkim_result) {
    where(dkim_result: dkim_result)
  }

  search_scope :spf_domain, -> (domain) {
    where(spf_domain: domain)
  }

  search_scope :spf_result, -> (spf_result) {
    where(spf_result: spf_result)
  }

  search_scope :disposition, -> (disposition) {
    where(disposition: disposition)
  }

  search_scope :predicted_disposition, -> (predicted_disposition) {
    where(predicted_disposition)
  }

  search_scope :header_from, -> (domain) {
    where(header_from: domain)
  }

  search_scope :network, -> (cidr) {
    where(Arel.sql("source_ip << cidr '#{cidr}'"))
  }

  search_scope :asn, -> (asn) {
    if asn =~ /^[0-9]+$/
      where(asn: asn)
    else
      where(aso: asn)
    end
  }

  search_scope :country, -> (country) {
    where(country_iso_code: country.upcase)
  }

  search_scope :start_date, -> (start_date) {
    joins(:report).where('reports.begin >= ?', start_date )
  }

  search_scope :end_date, -> (end_date) {
    joins(:report).where('reports.end <= ?', end_date )
  }



  def as_json
    super.merge({
        source_ip_string: self.source_ip_before_type_cast,
        autonomous_system: self.autonomous_system
      })
  end

  # Grouped totals
  # Confirmed working
  def self.total_by_country(limit=nil)
    if limit.nil?
      group(:country_name).
      order(Arel.sql('SUM(count) DESC')).
      pluck(:country_name, Arel.sql('SUM(count)'))
    else
      group(:country_name).
      order(Arel.sql('SUM(count) DESC')).
      pluck(:country_name, Arel.sql('SUM(count)')).
      first(limit)
    end
  end

  # Confirmed working
  def self.total_by_asn(limit=nil)
    if limit.nil?
      group(:aso, :asn).
      order(Arel.sql('SUM(count) DESC')).
      pluck(:aso, Arel.sql('SUM(count)'), :asn)
    else
      group(:aso, :asn).
      order(Arel.sql('SUM(count) DESC')).
      pluck(:aso, Arel.sql('SUM(count)'), :asn).
      first(limit)
    end
  end

  def self.total_by_header_from(limit=nil)
    if limit.nil?
      group(:header_from).
      order(Arel.sql('SUM(count) DESC')).
      pluck(:header_from)
    else
      group(:header_from).
      order(Arel.sql('SUM(count) DESC')).
      pluck(:header_from, Arel.sql('SUM(count)')).
      first(limit)
    end
  end

  def self.total_by(attr, limit=nil)
    if limit.nil?
      group(attr).
      order(Arel.sql('SUM(count) DESC')).
      pluck(attr)
    else
      group(attr).
      order(Arel.sql('SUM(count) DESC')).
      pluck(attr, Arel.sql('SUM(count)')).
      first(limit)
    end
  end

  # Confirmed working.
  def self.spf_stats()
    group(:header_from, :spf_domain, :spf_result, :country_name, :country_iso_code).order(Arel.sql('SUM(count) DESC')).pluck(:header_from, :spf_domain, :spf_result, :country_name, :country_iso_code, Arel.sql('SUM(count)'))
  end

  # Confirmed working
  def self.by_asn(asn)
    where(asn: asn)
  end

  # Confirmed working
  def self.by_spf_domain(domain)
    where(spf_domain: domain.downcase)
  end

  # Confirmed working
  def self.by_dkim_domain(domain)
    where(dkim_domain: domain.downcase)
  end

  # Confirmed working
  def self.by_country_code(country_code)
    where(country_iso_code: country_code.upcase)
  end

  # Confirmed working
  def self.by_header_from(domain)
    where(header_from: domain.downcase)
  end

  def self.by_disposition(disposition)
  end

  private
  # Callbacks
  def downcase_fields
    self.dkim_result.downcase! unless self.dkim_result.blank?
    self.dkim_domain.downcase! unless self.dkim_domain.blank?
    self.spf_result.downcase! unless self.spf_result.blank?
    self.spf_domain.downcase! unless self.spf_domain.blank?
    self.disposition.downcase! unless self.disposition.blank?
  end

  def fix_dkim
    self.dkim_result = "none" if self.dkim_result == ""
    self.dkim_domain = "none" if self.dkim_domain == ""
  end

  def fix_spf
    self.spf_result = "none" if self.spf_result == ""
    self.spf_domain = "none" if self.spf_domain == ""
    self.spf_result = "temperror" if self.spf_result == "unknown"
    self.spf_result = "permerror" if self.spf_result == "error"
  end

  def predict_dmarc
    self.predicted_disposition = false if self.spf == 'fail' and self.dkim == 'fail'
  end
end
