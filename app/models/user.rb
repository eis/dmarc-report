class User < ApplicationRecord
  self.primary_key = :netid

  has_secure_password validations: false

  def self.current_user
    @current_user
  end
end
