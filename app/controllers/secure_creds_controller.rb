class SecureCredsController < AdminController

  EDITABLE_FIELDS = [
    :tag,
    :username,
    :is_key,
    :password,
    :password_confirmation,
  ]

  def index
    @creds = SecureCred.order(:tag)
  end

  def new
    @secure_cred = SecureCred.new
  end

  def edit
    @secure_cred = SecureCred.find(params[:id])
  end

  # POST
  def create
    @secure_cred = SecureCred.new(params.require(:secure_cred).permit(*EDITABLE_FIELDS))

    if @secure_cred.save
      redirect_to admin_path, notice: 'Secure Credential was successfully created.'
    else
      render action: 'new'
    end
  end

  # PUT
  def update
    @secure_cred = SecureCred.find(params[:id])

    if @secure_cred.update_attributes(params.require(:secure_cred).permit(*EDITABLE_FIELDS))
      redirect_to admin_path, notice: 'Secure Credential was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @secure_cred = SecureCred.find(params[:id])
    if @secure_cred.destroy
      redirect_to admin_path, notice: 'Secure Credential was successfully removed.'
    else
      render action: 'index'
    end
  end
end
