class SearchController < ApplicationController
  def results
    @records = Record.filter(filter_params)
  end

  def show
    @records = Record.filter(filter_params)
  end

  def new

  end
  private

  def filter_params
    params.slice(*Record.search_scopes)
  end
end
