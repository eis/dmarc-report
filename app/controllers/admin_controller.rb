class AdminController < ApplicationController

  before_action :require_admin

  def index
    @services = Service.all
    @secure_creds = SecureCred.all
    @networks = Network.all
  end
end
