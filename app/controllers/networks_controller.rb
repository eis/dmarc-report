class NetworksController < ApplicationController
  EDITABLE_FIELDS = [
    :cidr,
    :display_name,
    :source,
    :status
  ]
  def index
    @records = Record.filter(filter_params)
  end

  def new
    @network = Network.new
  end

  def edit
    @network = Network.find(params[:id])
  end

  # POST
  def create
    @network = Network.new(params.require(:network).permit(*EDITABLE_FIELDS))

    if @network.save
      redirect_to "#{admin_path}#networks", notice: 'Network was successfully created.'
    else
      render action: 'new'
    end
  end

  # PUT
  def update
    @network = Network.find(params[:id])

    if @network.update_attributes(params.require(:network).permit(*EDITABLE_FIELDS))
      redirect_to "#{admin_path}#networks", notice: 'Network was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @network = Network.find(params[:id])
    if @network.destroy
      redirect_to "#{admin_path}#networks", notice: 'Network was successfully removed.'
    else
      render action: 'index'
    end
  end

  private

  def filter_params
    params.slice(*Record.search_scopes)
  end
end
