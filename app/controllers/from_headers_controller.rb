class FromHeadersController < ApplicationController
  def index
    #@from_headers_spf_pass = Record.where(spf: 'pass').group(:header_from).pluck(:header_from, Arel.sql('sum(count)'))
    #@from_headers_spf_fail = Record.where(spf: 'fail').group(:header_from).pluck(:header_from, Arel.sql('sum(count)'))
    #@from_headers_dkim_pass = Record.where(spf: 'pass').group(:header_from).pluck(:header_from, Arel.sql('sum(count)'))
    #@from_headers_dkim_fail = Record.where(spf: 'fail').group(:header_from).pluck(:header_from, Arel.sql('sum(count)'))
    records = Record.group(:header_from, :spf, :dkim).pluck(:header_from, :spf, :dkim, Arel.sql('sum(count)'))
    @from_headers = {}
    records.each do |domain, spf, dkim, count|
      if not @from_headers.key?(domain)
        @from_headers[domain] = HashWithIndifferentAccess.new({
          'dmarc_pass': 0,
          'dmarc_fail': 0,
          'spf_pass': 0,
          'spf_fail': 0,
          'dkim_pass': 0,
          'dkim_fail': 0,
          'total': 0
        })
      end
      @from_headers[domain]["spf_#{spf}"] += count
      @from_headers[domain]["dkim_#{dkim}"] += count
      if spf == 'fail' and dkim == 'fail'
        @from_headers[domain]['dmarc_fail'] += count
      else
        @from_headers[domain]['dmarc_pass'] += count
      end
      @from_headers[domain]['total'] += count
    end
  end

  def show
    @records = Record.where(header_from: params[:domain]).take
  end
end
