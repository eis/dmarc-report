class RecordController < ApplicationController
  def index
    #@records = Record.all.paginate(page: params[:page], per_page: 30)
    @records = Report.find(1041).records
    #@records = Record.all
    respond_to do |format|
      format.html
      format.json {render json: @records.as_json}
    end
  end

  def show
    @record = Record.find(params[:id])
    @report = @record.report
    @policy_published = @report.policy_published
    respond_to do |format|
      format.html
      format.json { render json: @record.as_json( methods: :source_ip_string, include: {
        report: {
          include: :policy_published } } ) }
    end
  end

  def dt

    @draw = params[:draw].to_i
    @recordstotal = Record.count
    @recordsFiltered = Record.count
  end

  private

  def dt_params
    params.permit(:draw, :start, :length, :search, :columns)
  end
end
