class AutonomousSystemController < ApplicationController
  def index
    @asns = AutonomousSystem.where.not(number: nil)
    records = Record.where.not(asn: nil).group(:asn, :aso, :spf, :dkim).pluck(:asn, :aso, :spf, :dkim, Arel.sql('sum(count)'))
    @asns = {}
    records.each do |asn, aso, spf, dkim, count|
      if not @asns.key?(asn)
        @asns[asn] = HashWithIndifferentAccess.new({
          'name': "#{aso} (#{asn})",
          'dmarc_pass': 0,
          'dmarc_fail': 0,
          'spf_pass': 0,
          'spf_fail': 0,
          'dkim_pass': 0,
          'dkim_fail': 0,
          'total': 0
        })
      end
      @asns[asn]["spf_#{spf}"] += count
      @asns[asn]["dkim_#{dkim}"] += count
      if spf == 'fail' and dkim == 'fail'
        @asns[asn]['dmarc_fail'] += count
      else
        @asns[asn]['dmarc_pass'] += count
      end
      @asns[asn]['total'] += count
    end
  end

  def show
    @asn = AutonomousSystem.where(number: params[:asn]).take
    #records = Record.where(asn: params[:asn])
    #@total = records.sum(:count)
    #@spf = records.group(:spf).order(:spf).reverse_order.sum(:count)
    #@dkim = records.group(:dkim).order(:dkim).reverse_order.sum(:count)
    #@dmarc_actual = records.group(:disposition).sum(:count)
    #@dmarc_predicted = {pass: 0, fail: 0}
    #records.group(:spf, :dkim).pluck(:spf, :dkim, Arel.sql('sum(count)')).each do |spf, dkim, count|
    #  if spf == 'fail' and dkim == 'fail'
    #    @dmarc_predicted[:fail] += count
    #  else
    #    @dmarc_predicted[:pass] += count
    #  end
    #end

    #@asn = Record.
    #  by_asn(params[:asn]).
    #  group(:header_from).
    #  order(Arel.sql('SUM(count) DESC')).
    #  pluck(:header_from, Arel.sql('SUM(count)')).
    #  paginate(page: params[:page], per_page: 30)
  end
end
