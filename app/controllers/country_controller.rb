class CountryController < ApplicationController

  def index
    records = Record.
      where.not(country_iso_code: nil).
      group(:country_iso_code, :country_name, :spf, :dkim).
      pluck(:country_iso_code, :country_name, :spf, :dkim, Arel.sql('sum(count)'))
    @countries = {}
    records.each do |country_iso_code, country_name, spf, dkim, count|
      if not @countries.key?(country_iso_code)
        @countries[country_iso_code] = HashWithIndifferentAccess.new({
          'name': "#{country_name} (#{country_iso_code})",
          'dmarc_pass': 0,
          'dmarc_fail': 0,
          'spf_pass': 0,
          'spf_fail': 0,
          'dkim_pass': 0,
          'dkim_fail': 0,
          'total': 0
        })
      end
      @countries[country_iso_code]["spf_#{spf}"] += count
      @countries[country_iso_code]["dkim_#{dkim}"] += count
      if spf == 'fail' and dkim == 'fail'
        @countries[country_iso_code]['dmarc_fail'] += count
      else
        @countries[country_iso_code]['dmarc_pass'] += count
      end
      @countries[country_iso_code]['total'] += count
    end
  end

  def show
    country_iso_code = params[:country_iso_code]
    if country_iso_code =~ /\A[[:lower:]]+\z/
      redirect_to country_path(country_iso_code.upcase), status: :moved_permanently
    end
    @country = Record.where(country_iso_code: params[:country_iso_code]).pluck(:country_name, :country_iso_code).take(1)
  end
end
