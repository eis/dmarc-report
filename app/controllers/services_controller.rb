class ServicesController < ApplicationController

  EDITABLE_FIELDS = [
    :tag,
    :host,
    :port,
    :protocol,
    :secure_cred_id,
    :recipient,
    :active
  ]

  def index
    @services = Service.order(:protocol)
  end

  def new
    @service = Service.new
  end

  def edit
    @service = Service.find(params[:id])
  end

  def create
    @service = Service.new(service_params)
    if @service.save
      redirect_to admin_path, notice: 'Service was successfully created.'
    else
      render action: 'new'
    end
  end

  def update
    @service = Service.find(params[:id])

    if @service.update_attributes(service_params)
      redirect_to admin_path notice: 'Service was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @service = Service.find(params[:id])
    if @service.destroy
      redirect_to admin_path, notice: 'Service was successfully deleted.'
    else
      render action: 'index'
    end
  end

  def test_service
    @service = Service.find(params[:id])
    ProbeMailer.baseline(@service).deliver_now
  end

  private

  def service_params
    params.require(:service).permit(*EDITABLE_FIELDS)
  end
end
