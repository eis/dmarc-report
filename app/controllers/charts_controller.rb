class ChartsController < ApplicationController

  # Yields an array of counts by top 10 ASNs and unshifts Axis values
  # [["ASN", "Messages Delivered"],["Duke University", 82,000], ["Microsoft", "68,0000"]...]
  def top_asn(limit=nil)
    results = Record.total_by_asn(limit).each do |result|
      asn = result.pop
      result.push("#{view_context.root_url}asn/#{asn}")
    end
    results.unshift(["ASO", "Messages Delivered", "ASN Link"])
    render json: results
  end

  def dmarc_status_summary
    total_count = Record.sum(:count)
    results = Record.group(:spf, :dkim).pluck(:spf, :dkim, Arel.sql('SUM(count)'))
    results.map! { |spf, dkim, count| ["SPF #{spf.capitalize}/DKIM #{dkim.capitalize}", count]  }

    # set offset for small anything less than 10%
    results.each do |result|
      if result[1] * 100 / total_count < 10
        result.push(0.5)
      else
        result.push(0.0)
      end
    end
    results.unshift(["SPF/DKIM Status", "Count", "Offset"])
    render json: results
  end

  def asn_summary
    records = Record.filter(filter_params)
    render json: records.
      group(:aso).
      order(Arel.sql('SUM(count) DESC')).
      pluck(:aso, Arel.sql('SUM(count)')).
      unshift(["Autonomous System", "Count"])
  end

  def dmarc_actual_summary
    records = Record.filter(filter_params)
    render json: records.
      group(:disposition).
      order(Arel.sql('SUM(count) DESC')).
      pluck(:disposition, Arel.sql('SUM(count)')).
      unshift(["DMARC Disposition", "Count"])
  end

  def dmarc_predicted_summary
    @dmarc_predicted = {pass: 0, fail: 0}
    records = Record.filter(filter_params)
    records.
      group(:spf, :dkim).
      pluck(:spf, :dkim, Arel.sql('sum(count)')).
      each do |spf, dkim, count|
        if spf == 'fail' and dkim == 'fail'
          @dmarc_predicted[:fail] += count
        else
          @dmarc_predicted[:pass] += count
        end
      end
    render json: @dmarc_predicted.to_a.unshift(["DMARC Status", "Count"])
  end

  def spf_summary
    records = Record.filter(filter_params)
    render json: records.
      group(:spf_result).
      order(Arel.sql('SUM(count)')).
      pluck(:spf_result, Arel.sql('SUM(count)')).
      unshift(["SPF Result", "Count"])
  end

  def dkim_summary
    records = Record.filter(filter_params)
    render json: records.
      group(:dkim_result).
      order(Arel.sql('SUM(count)')).
      pluck(:dkim_result, Arel.sql('SUM(count)')).
      unshift(["DKIM Result", "Count"])
  end

  def header_from_summary
    records = Record.filter(filter_params)
    render json: records.
      group(:header_from).
      order(Arel.sql('SUM(count)')).
      pluck(:header_from, Arel.sql('SUM(count)')).
      unshift(["From Header", "Count"])
  end

  def country_summary
    records = Record.filter(filter_params)
    render json: records.
      group(:country_iso_code).
      order(Arel.sql('SUM(count)')).
      pluck(:country_iso_code, Arel.sql('SUM(count)')).
      unshift(["Country Code", "Count"])
  end

  private

  def filter_params
    params.slice(*Record.search_scopes)
  end
end
