class ApplicationController < ActionController::Base

  layout 'ssiapp/application'

  before_action :authorize
  #around_action :set_current_user
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private


  def authorize
    # :nocov:
    request.env['REMOTE_USER'] = request.env['HTTP_REMOTE_USER'] if Rails.env.test? && request.env['REMOTE_USER'].blank?
    if (['development'].include? ENV['RAILS_ENV']) && (! request.env.include? 'REMOTE_USER')
      netid = 'dev_admin'
    else
    # :nocov:
      return denied if request.env['REMOTE_USER'].blank?
      netid, scope = request.env['REMOTE_USER'].split("@",2)
      return denied unless scope == 'duke.edu'
    end

    @user = User.find_or_initialize_by netid: netid
    @user.display_name = request.env['displayName'] unless request.env['displayName'].blank?
    @user.email_address = request.env['mail'] unless request.env['mail'].blank?
    @user.save if @user.changed?

    group_str = request.env['ismemberof']
    # :nocov:
    if Rails.env.test? && group_str.blank?
      group_str = request.env['HTTP_ISMEMBEROF']
    end
    # :nocov:
    groups = (group_str || '').split(';')
    unless groups.include?(Rails.configuration.access_group) || @user.admin
      denied
    end
  end

  def denied
    #ResourceLogger.info "Denied access for #{request.env['REMOTE_USER']} in group #{request.env['ismemberof']} to #{request.fullpath}"
    render 'home/no_access', layout: false, status: :forbidden
  end

  def require_admin
    denied unless @user.admin
  end

  def set_current_user
    User.as_user(@user) do
      yield
    end
  end
end
