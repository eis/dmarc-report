module LayoutHelper

  def application_name
    "DMARC Report"
  end

  def nav_links
    links = [
      {
        class: (params[:controller] == 'home' ) && 'active',
        link: link_to('Home', home_index_url)
      },
      #{
      #  class: (params[:controller] == 'record') && 'active',
      #  link: link_to('Records', record_index_url)
      #},
      {
        class: (params[:controller] == 'AutonomousSystem' ) && 'active',
        link: link_to('Autonomous System', autonomous_system_index_url)
      },
      {
        class: (params[:controller] == 'from_headers' ) && 'active',
        link: link_to("From Domain", from_headers_url)
      },
      {
        class: (params[:controller] == 'country' ) && 'active',
        link: link_to('Country', country_index_url)
      },
      #{
      #  class: (params[:controller] == 'networks') && 'active',
      #  link: link_to('Networks', networks_url)
      #},
      {
        class: (params[:controller] == 'search') && 'active',
        link: link_to('Explore', new_search_url)
      }
    ]
    links
  end

  #def nav_right
  #  render(partial: 'search/bar')
  #end

  def secondary_nav_links
    links = []
    links << link_to('Logout', "/Shibboleth.sso/Logout?return=https://shib.oit.duke.edu/cgi-bin/logout.pl")
    if @user.admin
      links.unshift link_to('Admin', admin_path)
    end
    links
  end

  def title_str
    "DMARC Report"
  end

end
