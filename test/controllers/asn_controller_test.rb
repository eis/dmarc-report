require 'test_helper'

class AsnControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get asn_index_url
    assert_response :success
  end

  test "should get show" do
    get asn_show_url
    assert_response :success
  end

end
