require 'test_helper'

class FromHeadersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get from_headers_index_url
    assert_response :success
  end

  test "should get show" do
    get from_headers_show_url
    assert_response :success
  end

end
