class AddGeoIpDataToRecords < ActiveRecord::Migration[5.2]
  def change
    add_column :records, :city, :string
    add_column :records, :continent_code, :string
    add_column :records, :continent_name, :string
    add_column :records, :country_iso_code, :string
    add_column :records, :country_name, :string
    add_column :records, :registered_country_iso_code, :string
    add_column :records, :registered_country_name, :string
    add_column :records, :latitude, :float
    add_column :records, :longitude, :float
    add_column :records, :accuracy_radius, :integer
  end
end
