class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users, id: false do |t|
      t.string :netid, null: false
      t.string :display_name
      t.string :email_address
      t.boolean :admin, null: false, default: false
      t.boolean :password_digest
      t.datetime :permissionExpiration
      t.boolean :service_account, null: false, default: false

      t.timestamps
    end
    add_index :users, :netid, unique: true
  end
end
