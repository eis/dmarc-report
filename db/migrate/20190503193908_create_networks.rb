class CreateNetworks < ActiveRecord::Migration[5.2]
  def change
    create_table :networks do |t|
      t.cidr :cidr
      t.string :display_name
      t.integer :source, default: 0
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
