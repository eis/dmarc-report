class AddActiveToServices < ActiveRecord::Migration[5.2]
  def change
    add_column :services, :active, :boolean
  end
end
