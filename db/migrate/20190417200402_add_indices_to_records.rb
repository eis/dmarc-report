class AddIndicesToRecords < ActiveRecord::Migration[5.2]
  def change
    add_index :records,
      [:autonomous_system_number, :autonomous_system_organization, :spf, :dkim, :disposition],
      name: 'index_records_on_asn_and_aso_and_spf_and_dkim_and_disposition'
    add_index :records,
      [:country_iso_code, :country_name, :spf, :dkim, :disposition],
      name: 'index_records_on_country_and_spf_and_dkim_and_disposition'
    add_index :records,
      [:header_from, :spf, :dkim, :disposition],
      name: 'index_records_on_from_and_spf_and_disposition'
  end
end
