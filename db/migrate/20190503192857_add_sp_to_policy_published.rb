class AddSpToPolicyPublished < ActiveRecord::Migration[5.2]
  def change
    add_column :policy_publisheds, :sp, :string
  end
end
