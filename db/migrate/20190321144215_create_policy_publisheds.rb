class CreatePolicyPublisheds < ActiveRecord::Migration[5.2]
  def change
    create_table :policy_publisheds do |t|
      t.string :domain
      t.string :adkim
      t.string :aspf
      t.string :p
      t.string :pct
      t.references :report, foreign_key: true

      t.timestamps
    end
  end
end
