class CreateAutonomousSystems < ActiveRecord::Migration[5.2]
  def change
    create_table :autonomous_systems do |t|
      t.integer :number
      t.string :name
      t.json :dkim_summary
      t.json :spf_summary

      t.timestamps
    end
  end
end
