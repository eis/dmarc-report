class AddTagToAutonomousSystem < ActiveRecord::Migration[5.2]
  def change
    add_column :autonomous_systems, :tag, :string
  end
end
