class AddAutonomousSystemDataToRecords < ActiveRecord::Migration[5.2]
  def change
    add_column :records, :autonomous_system_number, :string
    add_column :records, :autonomous_system_organization, :string
  end
end
