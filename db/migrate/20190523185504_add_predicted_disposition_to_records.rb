class AddPredictedDispositionToRecords < ActiveRecord::Migration[5.2]
  def change
    add_column :records, :predicted_disposition, :boolean, default: true
  end
end
