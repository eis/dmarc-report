class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :host
      t.integer :port
      t.integer :protocol
      t.references :secure_cred, foreign_key: true
      t.string :tag

      t.timestamps
    end
  end
end
