class CreateReports < ActiveRecord::Migration[5.2]
  def change
    create_table :reports do |t|
      t.string :org_name
      t.string :email
      t.string :extra_contact_info
      t.string :report_id
      t.datetime :begin
      t.datetime :end
      t.integer :failed_count
      t.integer :passed_count

      t.timestamps
    end
  end
end
