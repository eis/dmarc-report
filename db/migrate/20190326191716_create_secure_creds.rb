class CreateSecureCreds < ActiveRecord::Migration[5.2]
  def change
    create_table :secure_creds do |t|
      t.string :tag, null: false
      t.string :username
      t.boolean :is_key, default: false, null: false
      t.text :encrypted_password, null: false
      t.string :encrypted_password_iv, null: false
      t.string :encrypted_password_salt, null: false

      t.timestamps
    end
    add_index :secure_creds, :tag, unique: true
  end
end
