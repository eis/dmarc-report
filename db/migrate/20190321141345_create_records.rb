class CreateRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :records do |t|
      t.inet :source_ip
      t.string :dkim
      t.integer :count
      t.string :disposition
      t.string :spf
      t.string :reason_type
      t.string :comment
      t.string :header_from
      t.string :dkim_domain
      t.string :dkim_result
      t.string :spf_domain
      t.string :spf_result
      t.references :report, foreign_key: true

      t.timestamps
    end
  end
end
