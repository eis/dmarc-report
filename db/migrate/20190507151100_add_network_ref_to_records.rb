class AddNetworkRefToRecords < ActiveRecord::Migration[5.2]
  def change
    add_reference :records, :network, foreign_key: true
  end
end
