# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_23_185504) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "autonomous_systems", force: :cascade do |t|
    t.integer "number"
    t.string "name"
    t.json "dkim_summary"
    t.json "spf_summary"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tag"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "networks", force: :cascade do |t|
    t.cidr "cidr"
    t.string "display_name"
    t.integer "source", default: 0
    t.integer "status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "policy_publisheds", force: :cascade do |t|
    t.string "domain"
    t.string "adkim"
    t.string "aspf"
    t.string "p"
    t.string "pct"
    t.bigint "report_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sp"
    t.index ["report_id"], name: "index_policy_publisheds_on_report_id"
  end

  create_table "records", force: :cascade do |t|
    t.inet "source_ip"
    t.string "dkim"
    t.integer "count"
    t.string "disposition"
    t.string "spf"
    t.string "reason_type"
    t.string "comment"
    t.string "header_from"
    t.string "dkim_domain"
    t.string "dkim_result"
    t.string "spf_domain"
    t.string "spf_result"
    t.bigint "report_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "autonomous_system_number"
    t.string "autonomous_system_organization"
    t.string "city"
    t.string "continent_code"
    t.string "continent_name"
    t.string "country_iso_code"
    t.string "country_name"
    t.string "registered_country_iso_code"
    t.string "registered_country_name"
    t.float "latitude"
    t.float "longitude"
    t.integer "accuracy_radius"
    t.bigint "network_id"
    t.boolean "predicted_disposition", default: true
    t.index ["autonomous_system_number", "autonomous_system_organization", "spf", "dkim", "disposition"], name: "index_records_on_asn_and_aso_and_spf_and_dkim_and_disposition"
    t.index ["country_iso_code", "country_name", "spf", "dkim", "disposition"], name: "index_records_on_country_and_spf_and_dkim_and_disposition"
    t.index ["header_from", "spf", "dkim", "disposition"], name: "index_records_on_from_and_spf_and_disposition"
    t.index ["network_id"], name: "index_records_on_network_id"
    t.index ["report_id"], name: "index_records_on_report_id"
  end

  create_table "reports", force: :cascade do |t|
    t.string "org_name"
    t.string "email"
    t.string "extra_contact_info"
    t.string "report_id"
    t.datetime "begin"
    t.datetime "end"
    t.integer "failed_count"
    t.integer "passed_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "secure_creds", force: :cascade do |t|
    t.string "tag", null: false
    t.string "username"
    t.boolean "is_key", default: false, null: false
    t.text "encrypted_password", null: false
    t.string "encrypted_password_iv", null: false
    t.string "encrypted_password_salt", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tag"], name: "index_secure_creds_on_tag", unique: true
  end

  create_table "services", force: :cascade do |t|
    t.string "host"
    t.integer "port"
    t.integer "protocol"
    t.bigint "secure_cred_id"
    t.string "tag"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "active"
    t.index ["secure_cred_id"], name: "index_services_on_secure_cred_id"
  end

  create_table "users", id: false, force: :cascade do |t|
    t.string "netid", null: false
    t.string "display_name"
    t.string "email_address"
    t.boolean "admin", default: false, null: false
    t.boolean "password_digest"
    t.datetime "permissionExpiration"
    t.boolean "service_account", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["netid"], name: "index_users_on_netid", unique: true
  end

  add_foreign_key "policy_publisheds", "reports"
  add_foreign_key "records", "networks"
  add_foreign_key "records", "reports"
  add_foreign_key "services", "secure_creds"
end
