Rails.application.routes.draw do
  root 'home#index'
  get 'home/index'

  resources :autonomous_system, path: 'asn', param: :asn, only: [:index, :show]
  resources :country, param: :country_iso_code, only: [:index, :show]
  resources :from_headers, param: :domain , only: [:index, :show], constraints: { domain: /[^\/]+/ }
  resources :record, only: [:index, :show]
  resources :report, only: [:index, :show]
  resources :networks, except: [:show]
  resources :search, only: [:new]
  get 'search/results', to: 'search#results', as: :results_search
  get '/admin', to: 'admin#index', format: false
  scope :admin do
    resources :users
    resources :secure_creds, format: false, except: :show
    resources :services, format: false, except: :show do
      member do
        post :test_service
      end
    end
    resources :networks, format: false, except: :show
  end

  resource :charts do
    member do
      get 'asn_summary'
      get 'country_summary'
      get 'dkim_stats_by_asn'
      get 'dkim_stats_by_country'
      get 'dkim_summary'
      get 'dmarc_actual_summary'
      get 'dmarc_predicted_summary'
      get 'dmarc_status_summary'
      get 'header_from_summary'
      get 'spf_stats_by_asn'
      get 'spf_stats_by_country'
      get 'spf_summary'
      get 'top_asn'
    end
  end
end
